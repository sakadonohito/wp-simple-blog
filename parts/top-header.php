		<header class="site-main">
			<div class="header-image-main">
				<img src="<?php header_image();?>" alt="サイトのメインテーマ画像"/>
				<a class="site-title" href="<?php echo home_url();?>"><?php bloginfo('name');?></a>
			</div>
		</header>
