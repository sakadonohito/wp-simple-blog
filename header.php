<!doctype html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1"/>
	<title><?php bloginfo('name');?><?php wp_title(' : ');?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>" type="text/css" />
	<?php wp_head();?>
  </head>
  <body <?php body_class();?>>
    <div class="wrapper">
