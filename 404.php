<?php get_header();?>
        <?php get_template_part('parts/top-header');?>
		<div class="main-part">
			<main role="main">
				<div class="main-contents">
					<div class="main-content">
						<article>
							<header>
								<h1>Error:404</h1>
							</header>
							<section>
								<h3>Article not found.</h3>
								<p>could you please check url...</p>
							</section>
						</article>
					</div>
				</div>
			</main>
			<?php get_sidebar(); ?>
		</div>
		<?php get_footer();?>
