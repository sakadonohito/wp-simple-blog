        <?php get_header();?>
        <?php get_template_part('parts/top-header');?>
		<div class="main-part">
			<main role="main">
				<div class="main-contents">
					<header>
						<h1>アーカイブ: <?php echo get_the_date('Y年m月'); ?></h1>
					</header>
					<div class="article-list">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	                  <article>
                      <?php get_template_part('parts/list-articles');?>
					  </article>
                    <?php endwhile; endif; ?>
					</div>
				</div>
			</main>
			<?php get_sidebar(); ?>
		</div>
		<?php get_footer();?>
