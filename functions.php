<?php
//カスタムヘッダーの追加
$args = [
    'flex-width' => true,
    'flex-height' => true,
    'default-image' => get_template_directory_uri().'/images/top-header.png',
    'uploads' => true
];
add_theme_support('custom-header');
