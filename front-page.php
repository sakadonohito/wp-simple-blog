<?php get_header();?>
        <?php get_template_part('parts/top-header');?>
		<div class="main-part">
			<main role="main">
				<div class="main-contents">
					<header>
						<h1><a href="<?php echo home_url();?>"><?php bloginfo('name');?></a></h1>
						<p><?php bloginfo('description');?></p>
						<p>直近の記事数件</p>
					</header>
					<div class="article-list">
<?php
$args = array( 'posts_per_page' => 5, 'order'=> 'DESC', 'orderby' => 'date' );
$postslist = get_posts( $args );
foreach ( $postslist as $post ) : setup_postdata( $post );
?> 
						<article>
							<h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
							<p><?php the_excerpt(); ?></p>
						</article>
<?php
endforeach; 
wp_reset_postdata();
?>
					</div>
				</div>
			</main>
			<?php get_sidebar(); ?>
		</div>
		<?php get_footer();?>
