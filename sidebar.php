			<aside>
				<div class="side-menu">
					<section>
						<p>Categories</p>
						<ul>
<?php
$cats = get_categories();
if ($cats) {
    foreach($cats as $cat) {
        echo sprintf('<li><a href="%s">%s</a> (%d)</li>',get_category_link($cat->term_id),$cat->name,$cat->count);
    }
}
?>
						</ul>
					</section>
					<section>
						<p>Tags</p>
						<ul>
<?php
$tags = get_tags();
if ($tags) {
    foreach($tags as $tag) {
        echo sprintf('<li><a href="%s">%s</a> (%d)</li>',get_tag_link($tag->term_id),$tag->name,$tag->count);
    }
}
?>
						</ul>
					</section>
					<section>
						<p>Archives</p>
						<ul>
							<?php wp_get_archives(['show_post_count' => true]); ?>
						</ul>
					</section>
					<section>
						<p>Etc</p>
						<ul>
							<li><a href="/about">About</a></li>
						</ul>
					</section>
				</div>
			</aside>