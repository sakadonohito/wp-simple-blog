<?php get_header();?>
        <?php get_template_part('parts/top-header');?>
		<div class="main-part">
			<main role="main">
				<div class="main-contents">
					<article class="main-content">
<?php
if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
						<header>
							<h1><?php the_title(); ?></h1>
							<ul class="horizontal-list-over767">
								<li>Published: <?php the_date(); ?></li>
							</ul>
						</header>
						<section>
							<?php the_content(); ?>
						</section>
<?php
endwhile; endif;
?>
					</article>
				</div>
			</main>
            <?php get_sidebar(); ?>
		</div>
		<?php get_footer();?>
